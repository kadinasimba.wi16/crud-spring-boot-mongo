package com.Proyecto.Tienda.Entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Usuarios")
public class Student {
    @Id
    private String id;
    private String studentname;
    private String studentadress;
    private String mobile;


    //creo el constructor --> click derecho > generete > contrusctor >selecionamostodo
    public Student(String id, String studentname, String studentadress, String mobile) {
        this.id = id;
        this.studentname = studentname;
        this.studentadress = studentadress;
        this.mobile = mobile;
    }

    // aqui solo generemos solo student


    public Student() {

    }

    //get and set

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStudentname() {
        return studentname;
    }

    public void setStudentname(String studentname) {
        this.studentname = studentname;
    }

    public String getStudentadress() {
        return studentadress;
    }

    public void setStudentadress(String studentadress) {
        this.studentadress = studentadress;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    //verificaciones de errores y una cadena
    //click derecho > generar > Tostring
    @Override
    public String toString() {
        return "Student{" +
                "id='" + id + '\'' +
                ", studentname='" + studentname + '\'' +
                ", studentadress='" + studentadress + '\'' +
                ", mobile='" + mobile + '\'' +
                '}';
    }
}
