package com.Proyecto.Tienda.Entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Producto")
public class Product {
    @Id
    private String id;
    private String productname;
    private String productprice;
    private String categoria;
    private String descripcion;

    private String precio;

    public Product(String precio) {
        this.precio = precio;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    //genero los contructions
    public Product(String id, String productname, String productprice, String categoria, String descripcion) {
        this.id = id;
        this.productname = productname;
        this.productprice = productprice;
        this.categoria = categoria;
        this.descripcion = descripcion;
    }

    public Product() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }

    public String getProductprice() {
        return productprice;
    }

    public void setProductprice(String productprice) {
        this.productprice = productprice;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    //verificaciones de errores y una cadena
    //click derecho > generar > Tostring


    @Override
    public String toString() {
        return "Product{" +
                "id='" + id + '\'' +
                ", productname='" + productname + '\'' +
                ", productprice='" + productprice + '\'' +
                ", categoria='" + categoria + '\'' +
                ", descripcion='" + descripcion + '\'' +
                ", precio='" + precio + '\'' +
                '}';
    }
}
