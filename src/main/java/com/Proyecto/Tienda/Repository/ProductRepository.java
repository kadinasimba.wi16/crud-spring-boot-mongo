package com.Proyecto.Tienda.Repository;

import com.Proyecto.Tienda.Entity.Product;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ProductRepository extends MongoRepository<Product, String> {
}
