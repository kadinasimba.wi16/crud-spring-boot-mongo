package com.Proyecto.Tienda.Repository;

import com.Proyecto.Tienda.Entity.Student;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface StudentRepository extends MongoRepository<Student, String> {
}
