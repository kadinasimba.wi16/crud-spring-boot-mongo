#FROM ubuntu:latest
#LABEL authors="Kari"

#ENTRYPOINT ["top", "-b"]# Utiliza la imagen oficial de OpenJDK 11 como base
FROM openjdk:11-jre-slim

# Establece el directorio de trabajo en /app
WORKDIR /app

# Copia el archivo JAR de la aplicación al contenedor
COPY ./mi-aplicacion.jar .

# Expone el puerto 9001, que es el puerto predeterminado para las aplicaciones Spring Boot
EXPOSE 9001

# Comando para ejecutar la aplicación Spring Boot cuando el contenedor se inicia
CMD ["java", "-jar", "mi-aplicacion.jar"]

