package com.Proyecto.Tienda.Service;

import com.Proyecto.Tienda.Entity.Student;
import com.Proyecto.Tienda.Repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentService {

    @Autowired
    private StudentRepository repository;

    public void saveStudent(Student student) {
        repository.save(student);
    }

    public Iterable<Student> listAll(){
        return this.repository.findAll();
    }

    public void deleteStudent(String id){
        repository.deleteById(id);
    }
    public Student getStudentPorId(String studentid){

        return repository.findById(studentid).get();
    }

}
