package com.Proyecto.Tienda.Controller;
import com.Proyecto.Tienda.Entity.Product;
import com.Proyecto.Tienda.Service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@CrossOrigin(origins = "*")
@RequestMapping("Producto/")

public class ProductController {
    @Autowired
    private ProductService productService;  // Corregido el nombre de la variable



    @PostMapping(value = "/save")
    private String saveStudent(@RequestBody Product product) {
        productService.saveProduct(product);  // Corregido el nombre del método
        return product.getId();
    }

    @GetMapping("/probando")
    public String probando() {
        return "¡Hola desde el endpoint GET de PRODUCTOS!";
    }


    @GetMapping(value = "/getAll")
    private Iterable<Product>getStudents() {

        return productService.listAll();
    }

    //ahora el put para editar por un ID
    @PutMapping(value = "/edit/{id}")
    private Product update(@RequestBody Product product,@PathVariable(name = "id")String _id){

        product.setId(_id);
        productService.saveProduct(product);
        return product;
    }
    //ahora el delete para eliminar por un ID
    @DeleteMapping("/delete/{id}")
    private void deleteProduct(@PathVariable("id")String _id){
        productService.deleteProduct(_id);
    }
    //ahora el delete para eliminar por un ID
    @RequestMapping ("/student/{id}")
    private Product getProduct(@PathVariable(name="id")String productid){
        return productService.getProductPorId(productid);
    }

}
