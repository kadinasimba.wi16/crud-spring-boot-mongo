package com.Proyecto.Tienda.Service;

import com.Proyecto.Tienda.Entity.Product;
import com.Proyecto.Tienda.Repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
@Service
public class ProductService {
    @Autowired
    private ProductRepository repository;

    public void saveProduct(Product product) {
        repository.save(product);
    }

    public Iterable<Product> listAll(){
        return this.repository.findAll();
    }

    public void deleteProduct(String id){
        repository.deleteById(id);
    }
    public Product getProductPorId(String productid){

        return repository.findById(productid).get();
    }
}
