package com.Proyecto.Tienda.Controller;

import com.Proyecto.Tienda.Entity.Student;
import com.Proyecto.Tienda.Service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("api/v1/student")
public class StudentController {

    @Autowired
    private StudentService studentService;  // Corregido el nombre de la variable

    @PostMapping(value = "/save")
    private String saveStudent(@RequestBody Student student) {
        studentService.saveStudent(student);  // Corregido el nombre del método
        return student.getId();
    }

    @GetMapping("/probando")
    public String probando() {
        return "¡Hola desde el endpoint GET de clientes!";
    }


    @GetMapping(value = "/getAll")
    private Iterable<Student>getStudents() {

        return studentService.listAll();
    }

    //ahora el put para editar por un ID
    @PutMapping(value = "/edit/{id}")
    private Student update(@RequestBody Student student,@PathVariable(name = "id")String _id){

        student.setId(_id);
        studentService.saveStudent(student);
        return student;
    }
    //ahora el delete para eliminar por un ID
    @DeleteMapping("/delete/{id}")
    private void deleteStudent(@PathVariable("id")String _id){
        studentService.deleteStudent(_id);
    }
    //ahora el delete para eliminar por un ID
    @RequestMapping ("/student/{id}")
    private Student getStudent(@PathVariable(name="id")String studentid){
        return studentService.getStudentPorId(studentid);
    }

}
